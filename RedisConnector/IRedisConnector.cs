﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RedisConnector
{
    [ServiceContract(CallbackContract = typeof(IDuplexCallback))]
    public interface IRedisConnector
    {
        [OperationContract]
        void SetEntry(string key, string value);

        [OperationContract]
        void SetEntry(string key, string value, TimeSpan expiresIn);

        [OperationContract]
        string GetEntry(string key);

        [OperationContract]
        void DeleteEntry(string key);

        [OperationContract]
        long GetDbSize();

        [OperationContract]
        long IncrementValue(string key);

        [OperationContract]
        bool AddItemToSortedSet(string setId, string value);

        [OperationContract]
        long GetSortedSetCount(string setId);

        [OperationContract]
        long GetItemIndexInSortedSet(string setId, string value);

        [OperationContract]
        List<string> GetRangeFromSortedSet(string setId, int fromRank, int toRank);
    }
}
