﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using ServiceStack.Redis;

namespace RedisConnector
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class RedisConnector : IRedisConnector
    {
        private readonly string _redisHost;
        private readonly PooledRedisClientManager _redisManager;
        private readonly IRedisClient _redisClient;
        private readonly string _redisPassword;

        public RedisConnector()
        {
            NameValueCollection section = (NameValueCollection)ConfigurationManager.GetSection("redisSettings");
            _redisHost = section["redisHost"];
            _redisPassword = section["redisPassword"];
            _redisManager = new PooledRedisClientManager(_redisHost);
            _redisClient = _redisManager.GetClient();
            _redisClient.Password = _redisPassword;
        }

        public IDuplexCallback Callback
        {
            get { return OperationContext.Current.GetCallbackChannel<IDuplexCallback>(); }
        }

        public void SetEntry(string key, string value)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", key))
                    throw new Exception("Key character mismatch. Use only alphanumerical characters and underscores.");
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", value))
                    throw new Exception("Value character mismatch. Use only alphanumerical characters and underscores.");
                _redisClient.SetValue(key, value);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
            }
        }

        public void SetEntry(string key, string value, TimeSpan expiresIn)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", key))
                    throw new Exception("Key character mismatch. Use only alphanumerical characters and underscores.");
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", value))
                    throw new Exception("Value character mismatch. Use only alphanumerical characters and underscores.");
                _redisClient.SetValue(key, value, expiresIn);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
            }
        }

        public string GetEntry(string key)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", key))
                    throw new Exception("Key character mismatch. Use only alphanumerical characters and underscores.");
                return _redisClient.GetValue(key);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
                return null;
            }
        }

        public void DeleteEntry(string key)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", key))
                    throw new Exception("Key character mismatch. Use only alphanumerical characters and underscores.");
                _redisClient.RemoveEntry(key);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
            }
        }

        public long GetDbSize()
        {
            try
            {
                return _redisClient.DbSize;
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
                return -1;
            }
        }

        public long IncrementValue(string key)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", key))
                    throw new Exception("Key character mismatch. Use only alphanumerical characters and underscores.");
                return _redisClient.IncrementValue(key);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
                return -1;
            }
        }

        public bool AddItemToSortedSet(string setId, string value)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", setId))
                    throw new Exception("Set ID character mismatch. Use only alphanumerical characters and underscores.");
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", value))
                    throw new Exception("Value character mismatch. Use only alphanumerical characters and underscores.");
                return _redisClient.AddItemToSortedSet(setId, value);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
                return false;
            }
        }

        public long GetSortedSetCount(string setId)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", setId))
                    throw new Exception("Set ID character mismatch. Use only alphanumerical characters and underscores.");
                return _redisClient.GetSortedSetCount(setId);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
                return -1;
            }
        }

        public long GetItemIndexInSortedSet(string setId, string value)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", setId))
                    throw new Exception("Set ID character mismatch. Use only alphanumerical characters and underscores.");
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", value))
                    throw new Exception("Value character mismatch. Use only alphanumerical characters and underscores.");
                return _redisClient.GetItemIndexInSortedSet(setId, value);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
                return -1;
            }
        }

        public List<string> GetRangeFromSortedSet(string setId, int fromRank, int toRank)
        {
            try
            {
                if (!Regex.IsMatch(".*[^a-zA-Z0-9_].*", setId))
                    throw new Exception("Set ID character mismatch. Use only alphanumerical characters and underscores.");
                return _redisClient.GetRangeFromSortedSet(setId, fromRank, toRank);
            }
            catch (Exception e)
            {
                Callback.RedisConnectionIssues(e.Message);
                return null;
            }
        }
    }
}
