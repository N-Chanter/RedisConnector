﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace RedisConnector
{
    public interface IDuplexCallback
    {
        [OperationContract]
        void RedisConnectionIssues(string response);
    }
}
